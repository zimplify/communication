<?php
    namespace Zimplify\Rest\Middlewares;
    use Zimplify\Rest\Interfaces\IUploadableInterface;
    use Zimplify\Core\{Application, File};    
    use Slim\Http\Request;
    use Slim\Http\Response;

    /**
     * this middleware checks the X-Cendol-Device header and decode the identity of the device
     * @package Zimplify\Rest (code 05)
     * @type middleware (code 10)
     * @file: UploadHandlerMiddleware (code 01)
     */    
    class UploadHandlerMiddleware implements IUploadableInterface {

        const HDR_CONTENT_TYPE = "Content-Type";

        /**
         * the main function
         * @param Request $req the incoming request
         * @param Response $res the outgoing response
         * @param callable $next the function to jump next to
         * @return Response
         */
        public function __invoke(Request $req, Response $res, callable $next) : Response {
            // extract out the content type
            $content = $req->getHeader(self::HDR_CONTENT_TYPE);
            $path = Application::env(self::CFG_TMP_PATH);                    
            $files = [];

            // dealing with different content type
            switch ($content) {
                case "multipart/form-data":
                    foreach ($req->getUploadedFiles() as $f) {
                        $name = uniqid("", true);
                        move_uploaded_file($f["tmp_name"], $path."/".$name);

                        // now define the file instnace
                        $file = new File();
                        $file->location = $path."/".$name;
                        $file->mime = $f["type"];
                        $file->size = $f["size"];
                        $file->output = false;

                        // now store the file
                        array_push($files, $file);
                    }
                    $req = $req->withAttributes(self::ATTR_UPLOADS, $files);
                    break;
            }

            // now we continue with the flow
            $res = $next($req, $res);

            // let's clean the uploads
            foreach ($files as $f) 
                if (file_exists($f["name"])) unlink($f["name"]);            

            // doing out the middleware back to stack
            return $res;
        }
    }