<?php
    namespace Zimplify\Rest\Interfaces;

    /**
     * this interface allow us to enable controllers to indicate whether they contains uploads
     * @package Zimplify\Rest (code 051)
     * @type Interface (code 06)
     * @file IUploadableInterface (code 02)
     */    
    interface IUploadableInterface {

        const ATTR_UPLOADS = "uploads";
        const CFG_TMP_PATH = "system.upload.path";
        
    }