<?php
    namespace Zimplify\Rest\Interfaces;

    /**
     * this interface contains the common response code variables to save retyping
     * @package Zimplify\Security (code 05)
     * @type interface (code 6)
     * @file IResponseCodeInterface (code 01)
     */

    interface IResponseCodeInterface {

        const RES_SUCCESS = 200;
        const RES_ACCEPTED = 202;
        const RES_UPDATE_TOKEN = 208;
        const RES_BAD_REQUEST =  400;
        const RES_NOT_ACCEPTED =  401;
        const RES_NOT_AUTHORIZED = 403;
        const RES_NOT_FOUND = 404;
        const RES_NOT_ALLOWED = 403;
        const RES_INTERNAL_ERROR = 500;
        const RES_SESSION_EXPIRY = 504;

    }