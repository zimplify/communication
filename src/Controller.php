<?php
    namespace Zimplify\Rest;
    use Zimplify\Core\{Application, Instance};
    use Zimplify\Core\Services\XmlUtils;
    use Zimplify\Rest\{Alert, Reply};
    use Zimplify\Rest\Interfaces\{IResponseCodeInterface, IUploadableInterface};    
    use Zimplify\Security\Interfaces\IAuthenticateableInterface;
    use Slim\Psr7\{Request, Response};
    use Psr\Container\ContainerInterface;
    use \Exception;

    /**
     * this is the new Slim 4 route container that will allow us to save a lot of debug work for later
     * @package: Zimplify\Core (code 05);
     * @instance: Model (code: 01)
     * @file: Controller (code: 03)
     */
    abstract class Controller implements IResponseCodeInterface, IUploadableInterface{

        const ARGS_OFFSET = "offset";
        const ATTR_AGENT = "agent";
        const ATTR_OFFSET = "offset";
        const ATTR_SIZE = "size"; 
        const CFG_DEBUG = "application.debug";
        const DEF_PAGE_SIZE = 10;
        const DTYPE_DATE = "date";
        const FLD_EXCLUDED = "excluded";
        const FLD_NAME = "name";
        const FLD_TYPE = "type";        
        const XPATH_GET_METADATA = "//metadata";

        protected $body;
        protected $files = [];

        /**
         * creating the container
         * @param ContainerInterface $container the containers to apply to the controller
         */
        public function __construct(ContainerInterface $container) {
            $this->container = $container;
        }        

        /**
         * this is controller invoker so we do not need to rewrite the whole thing
         * @param Request $req the incoming request
         * @param Response $res the outgoing response
         * @param array $args (optional) the parameters came in
         * @return Response
         */
        public function __invoke(Request $req, Response $res, array $args = []) : Response {
            try {
                // loading all the related data
                $this->body = $req->getParsedBody();
                $this->files = $req->getAttribute(self::ATTR_UPLOADS) ?? [];
                $qps = $req->getQueryParams();
                $offset = 0;

                // TR-003 need to add the paging offset here
                if (array_key_exists(self::ARGS_OFFSET, $qps)) {
                    $offset = (int) $qps[self::ARGS_OFFSET];
                    $req = $req->getAttribute(self::ATTR_OFFSET, $offset);
                }

                // adding the page size to make sure query picks up
                $req = $req->getAttribute(self::ATTR_SIZE, $offset > 0 ? self::DEF_PAGE_SIZE : 0);
                // END: TR-003

                // now run the main request processings
                $result = $this->process($req);      
            } catch (Exception $ex) {
                $this->log($ex);
                if (Application::env(self::CFG_DEBUG)) 
                    throw $ex;
                else 
                    $result = new Alert($ex);
            } finally {
                return $result->flush();
            }
        }

        /**
         * detecthing the data structure within the node
         * @param Instance $source the data source
         * @param SimpleXMLElement $node the node use to extract data
         * @param IAuthenticateableInterface $user the user that helps to check the data
         * @return mixed
         */
        protected static function detect(Instance $source, SimpleXMLElement $node, IAuthenticateableInterface $user = null) {
            $result = null;
            $a = XmlUtils::attributes($node);

            // check if the data is excluded
            if (!static::isExcluded($a, $user)) {
                if (count($node->children()) > 0) {
                    foreach ($node->children() as $n) {
                        $na = XmlUtils::attributes($n);
                        $f = $na[self::FLD_NAME];
                        $result[$f] = static::detect($source, $n, $user);
                    }
                } else {
                    $value = $source->{XmlUtils::translate($node)};
                    if (!is_null($value)) 
                        $result = $a[self::FLD_TYPE] == self::DTYPE_DATE ? DataUtils::uncast($a[self::FLD_TYPE], $value) : $value; 
                    else 
                        if ($a[Instance::DS_TYPE] === "yesno") 
                            $result = false;
                }
            }

            // return the result
            return $result;                
        }           

        /**
         * generating the result dataset from the original data
         * @param Instance $source the source dataset
         * @param IAuthenticateableInterface $user the user to evaluate exclusions
         * @return array 
         */
        public static function display(Instance $source, IAuthenticateableInterface $user = null) : array {
            $result = [];

            // first checked out the metadata section
            foreach ($source->describe()->xpath(self::XPATH_GET_METADATA) as $m) {

                // dealing with the first layer node
                foreach ($m->children() as $n) {

                    // loading out the attributes
                    $a = XmlUtils::attributes($n);
                    $f = $a[self::FLD_NAME];

                    // now we need to check if the data to be excluded
                    if (!static::isExcluded($a, $user)) {
                        if (count($n->children()) > 0) {
                            $value = static::detect($source, $n, $user);
                        } else {
                            $value = $source->{XmlUtils::translate($n)};
                        }                        

                        // now we need to transform the data if is a date
                        if (is_object($value) && substr_count(get_class($value), "DateTime") == 1) 
                            $value = $value->format("U") * 1000;
                        else if (array_key_exists(Instance::DS_TYPE, $a) && $a[Instance::DS_TYPE] == "yesno") 
                            $value = $value === true ? true : false;
                        
                        // now porting the value out
                        $result[$f] = $value;
                    }                    
                }
            }

            // updating date for created - need to be in epoch milliseconds
            if (array_key_exists("created", $result)) 
                $result["created"] = $source->created->format("U") * 1000;

            // updated date correction - use created if not provided
            if (array_key_exists("updated", $result) && $result["updated"] == 0) 
                $result["updated"] = $source->created->format("U") * 1000;

            // now return the structure
            return $result;
        }

        /**
         * see if the node needs to be excluded or otherwise
         * @param array $attributes the node attributes
         * @param IAuthenticateableInterface $user the user asking for the data
         * @return bool
         */
        protected static function isExcluded(array $attributes, IAuthenticateableInterface $user = null) : bool {
            $result = false;

            // make sure we have the exclude field
            if (array_key_exists(self::FLD_EXCLUDED, $attributes) && $user) 
                
                // now switch bsed on the field
                switch ($attributes[self::FLD_EXCLUDED]) {

                    // if is for everyone then return
                    case "all": $result = true; break;

                    // detect if any of the roles in the excluded list
                    default:                        
                        foreach ($user->{IAuthenticateableInterface::FLD_ROLES} as $r) {
                            $result = substr_count($attributes[self::FLD_EXCLUDED], $r) > 0;
                            break;
                        }
                }

            // return the result
            return $result;            
        }        

        /**
         * logging the errors in a nice way...
         * @param Exception $ex the exception thrown
         * @return void
         */
        private function log(Exception $ex) {
            $controller = end(explode("\\", get_class($this)));
            $message = "ERR-$controller> ".$ex->getMessage()." [".$ex->getCode()."]";
            error_log($message);            
        }

        /**
         * this is the real function that processes the work
         * @param Request $req the request that triggers the work
         * @param array $args (optional) the values got from the URL dissect
         * @return Reply 
         */
        protected abstract function process(Request $req, array $args = []) : Reply;

    }